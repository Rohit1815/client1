package com.example.client.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.discovery.EurekaClient;

@RestController
public class MainController {
	
	@Autowired
    private EurekaClient discoveryClient;
	
	@GetMapping("/sample")
	public String getData() {
		System.out.println("reached this part of the code");
		JSONObject returnObj=new JSONObject();
		returnObj.put("test", "test1");
		return returnObj.toString();
	}
	@GetMapping("/sampleClient2")
	public String client2Data() {
		System.out.println("data from client2");
		RestTemplate restTemplate=new RestTemplate();
		String url=discoveryClient.getNextServerFromEureka("CLIENT2", false).getHomePageUrl();
		String client2String=restTemplate.getForEntity(url+"/sampleTest", String.class).getBody();
		return client2String;
	}

}
